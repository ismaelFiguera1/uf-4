package cat.almata.a016;

public class Microprocesssador {
	private String frequencia;

	public String getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(String frequencia) {
		this.frequencia = frequencia;
	}

	public Microprocesssador(String frequencia) {
		super();
		setFrequencia(frequencia);
	}
	
	
}
