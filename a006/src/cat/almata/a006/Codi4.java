package cat.almata.a006;

import java.util.Scanner;

public class Codi4 {
	private String text;
	private String paraula;
	private int contador;
	
	
	
	
	
	public Codi4() {
		setText();
		setParaula();
		buscarParaula();
		System.out.println("La paraula surt "+contador+" vegades");
	}
	public String getText() {
		System.out.println("El text es:\t"+text);
		return text;
	}
	public void setText() {
		demanarText();
		if (text.length()>100) this.text="";
	}
	public String getParaula() {
		return paraula;
	}
	public void setParaula() {
		paraula = "home";
	}
	public int getContador() {
		return contador;
	}
	public void setContador() {
		contador = 0;
	}
	
	public void demanarText() {
		Scanner a = new Scanner(System.in);
		System.out.println("Escriu el text:\t");
		text = a.nextLine();
		a.close();
	}
	
	public int buscarParaula() {
		setContador();
		String text1 = text.toLowerCase();		//	Passo tot el text a minuscules
		String[] arrayParaulesText;
		arrayParaulesText = text1.split("[\\s,.!?]+");		//	Separo totes les paraules del text mitxançant espais, punts i comes, simbols d'exclamacio ...
		
		for (int i=0; i<arrayParaulesText.length;i++) {
			if(arrayParaulesText[i].equals(paraula)) contador++;
		}
		return contador;
	}
	
	
}
