package cat.almata.a018;

import cat.almata.a018.PlacaBase.XipSet;

public class Ppal {
	public static void main(String[] args) {
		PlacaBase p1 = new PlacaBase();
		XipSet x1 = PlacaBase.getXipSet();
		
		Processador pr1 = new Processador("LGA1366", "1");
		// Comprobar que el microprocessador es compatible amb el xipset
		if(x1.getSocolProcessador().equals(pr1.getSocol())) {
			System.out.println("Tot es correcte, afegir el modul");
		}else System.out.println("El processador NO es compatible");
		
		Memoria m1 = new Memoria("1333", "1");
		
		if(x1.getFrequenciaRAM().equals(m1.getFrequencie())) {
			System.out.println("Es tot correcte");
		}else System.out.println("La memoria NO es compatible");
		
//		Integer edat2 = Integer.valueOf(44);
		
//		p1=null;	Esborra placaBase i xipsep
	}
}
