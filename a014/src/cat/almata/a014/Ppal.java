package cat.almata.a014;

import java.util.ArrayList;

public class Ppal {

	public static void main(String[] args) {
		ArrayList<Persona> persones = new ArrayList<Persona>();
		
		Persona p1 = new Persona("Suarez");
		Persona p2 = new Persona("Messi");
		Persona p3 = new Persona("Iniesta");
		Persona p4 = new Persona("Xavi");
		
		Camio c1 = new Camio("a001");
		Camio c2 = new Camio("a201");
		Camio c3 = new Camio("a3401");
		Camio c4 = new Camio("a0541");
		
		persones.add(p1);
		persones.add(p2);
		persones.add(p3);
		persones.add(p4);
		
		for(Persona p:persones) {
			System.out.println(p.getNom());
		}
		
		p1.setNom("Neymar");
		System.out.println("He canviat un nom");
		for(Persona p:persones) {
			System.out.println(p.getNom());
		}

	}
	


}
