package cat.almata.a018;

public class PlacaBase {
	//(***)
	private static XipSet xipSet=null;
	
	private static PlacaBase placaBase;
	
	public PlacaBase() {
		placaBase=this;
		xipSet = XipSet.obtenirXipSet();
	}
	
	
	public static XipSet getXipSet() {
		return xipSet;
	}


	public static PlacaBase getPlacaBase() {
		return placaBase;
	}


	/**
	 * 	Relacio composicio entre PlacaBase i XipSet
	 * 	La vida de XipSet depen de PlacaBase
	 * 	Aplicarem el patro Singleton. Assegura que nomes hi hagi una sola instancia
	 * de la classe XipSet
	 */
	public class XipSet {
		private String socolProcessador;
		private String frequenciaRAM;
		
		//(*)
		
		private XipSet() {
			socolProcessador="LGA1366";
			frequenciaRAM="1333";
		}
		
		//(**)
		private static XipSet obtenirXipSet() {
			//(***)
			if(xipSet==null) xipSet = placaBase.new XipSet();
			return xipSet;
		}

		public String getSocolProcessador() {
			return socolProcessador;
		}

		public String getFrequenciaRAM() {
			return frequenciaRAM;
		}
		
		
		
	}
}
