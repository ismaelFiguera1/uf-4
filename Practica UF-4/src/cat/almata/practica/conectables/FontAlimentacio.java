package cat.almata.practica.conectables;

import cat.almata.practica.PlacaBase.XipSet;
import cat.almata.practica.instalables.InstalablePB;

public class FontAlimentacio implements InstalablePB {

	private int potencia;
	private String connector;
	private String id;
	
	
	
	
	public FontAlimentacio(int potencia, String connector, String id) {
		super();
		setPotencia(potencia);
		setConnector(connector);
		setId(id);
	}




	public int getPotencia() {
		return potencia;
	}




	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}




	public String getConnector() {
		return connector;
	}




	public void setConnector(String connector) {
		this.connector = connector;
	}




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	@Override
	public String toString() {
		return "FontAlimentacio [potencia=" + potencia + ", connector=" + connector + ", id=" + id + "]";
	}




	@Override
	public boolean isConnectable(XipSet x, Object a) {
		boolean ser;
		FontAlimentacio f1 = (FontAlimentacio) a;
		if(f1.getConnector().equals(x.getConnectorFont())) {
			ser = true;
		}else {
			ser = false;
		}
		return ser;
	}

}
