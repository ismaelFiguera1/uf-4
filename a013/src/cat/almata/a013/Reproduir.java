package cat.almata.a013;

public interface Reproduir{
	public void play();
	public void stop();
	public void next();
	public void pause();
	public void previous();
}
