package cat.almata.practica.conectables;

import cat.almata.practica.PlacaBase.XipSet;
import cat.almata.practica.instalables.InstalablePB;

public class DiscDur implements InstalablePB {
	private double capacitat;
	private String connector;
	private String id;
	
	
	
	
	public DiscDur(double capacitat, String connector, String id) {
		super();
		setCapacitat(capacitat);
		setConnector(connector);
		setId(id);
	}




	@Override
	public String toString() {
		return "DiscDur [capacitat=" + capacitat + ", connector=" + connector + ", id=" + id + "]";
	}




	public double getCapacitat() {
		return capacitat;
	}




	public void setCapacitat(double capacitat) {
		this.capacitat = capacitat;
	}




	public String getConnector() {
		return connector;
	}




	public void setConnector(String connector) {
		this.connector = connector;
	}




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	@Override
	public boolean isConnectable(XipSet x, Object a) {
		boolean ser;
		DiscDur d1 = (DiscDur) a;
		if(d1.getConnector().equals(x.getConnectorDiscDur())) {
			ser = true;
		}else {
			ser = false;
		}
		return ser;
	}

}
