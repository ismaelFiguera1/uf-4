package cat.almata.a011;

public class Ppal {

	public static void main(String[] args) {
		Persona p1 = new Esportista("Runner", "Alex", "02010306Ñ");
		Persona p2 = new Persona();
		Persona p3 = new Esportista("Alpinisme", "Messi", "02010966Ñ");
		Persona p4 = new Professional("Alef", "002", 235);
		p3.toString();
		
		posarACorrerAPersones(p1);
		posarACorrerAPersones(p2);
		posarACorrerAPersones(p3);
		posarACorrerAPersones(p4);
	}
	
	public static void posarACorrerAPersones(Persona p) {
		p.correr();
	}

}
