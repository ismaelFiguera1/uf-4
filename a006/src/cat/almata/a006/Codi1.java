package cat.almata.a006;

import java.util.Scanner;

public class Codi1 {
	private String paraula;
	private String paraula1;
	
	
	public Codi1() {
		setParaula();
		getParaula();
		setParaula1();
		getParaula1();
		resultat();
		
	}
	
	
	public String getParaula() {
		System.out.println("La paraula es "+paraula +".");
		return paraula;
	}

	public String getParaula1() {
		System.out.println("La paraula es "+paraula1);
		return paraula1;
	}

	public void setParaula() {
		Scanner entrada = new Scanner(System.in);
		System.out.println("\nEntra una paraula:\t");
		paraula=entrada.next();
		entrada.close();
	}
	
	public void setParaula1() {
		Scanner entrada = new Scanner(System.in);
		System.out.println("\nEntra una paraula:\t");
		paraula1=entrada.next();
		entrada.close();
	}
	
	public boolean comparar() {
		if(paraula.equalsIgnoreCase(paraula1)) return true;
		else return false;
	}
	
	public void resultat() {
		boolean a = comparar();
		if(a==true) System.out.println("Felicitats l'has trobat.");
		else System.out.println("No l'has trobat, la proxima sera.");
	}
}
