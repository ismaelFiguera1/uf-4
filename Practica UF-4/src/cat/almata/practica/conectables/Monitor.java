package cat.almata.practica.conectables;

import cat.almata.practica.PlacaBase.XipSet;
import cat.almata.practica.instalables.InstalablePB;

public class Monitor implements InstalablePB {

	private double frequencia;
	private double polsades;
	private String connector;
	private String id;
	
	
	
	
	
	
	
	
	
	
	
	
	public double getFrequencia() {
		return frequencia;
	}












	public void setFrequencia(double frequencia) {
		this.frequencia = frequencia;
	}












	public double getPolsades() {
		return polsades;
	}












	public void setPolsades(double polsades) {
		this.polsades = polsades;
	}












	public String getConnector() {
		return connector;
	}












	public void setConnector(String connector) {
		this.connector = connector;
	}












	public String getId() {
		return id;
	}












	public void setId(String id) {
		this.id = id;
	}












	public Monitor(double frequencia, double polsades, String connector, String id) {
		super();
		setFrequencia(frequencia);
		setPolsades(polsades);
		setConnector(connector);
		setId(id);
	}












	@Override
	public String toString() {
		return "Monitor [frequencia=" + frequencia + ", polsades=" + polsades + ", connector=" + connector + ", id="
				+ id + "]";
	}












	@Override
	public boolean isConnectable(XipSet x, Object a) {
		boolean ser;
		Monitor m1 = (Monitor) a;
		if(m1.getConnector().equals(x.getConnectorMonitor())) {
			ser = true;
		}else {
			ser = false;
		}
		return ser;
	}

}
