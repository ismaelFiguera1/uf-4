package cat.almata.a002;

public class Elefant {
	private int edat;
	private String genere;
	private boolean captivitat;
	private float pes;
	private float altura;
	
	public void beurer() {
		System.out.println("El elefant esta bebent aigua");
	}
	
	public void caminar() {
		System.out.println("El elefant esta caminant");
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int num) {
		if (num<100) this.edat = num;
		else this.edat=0;
		
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public boolean isCaptivitat() {
		return captivitat;
	}

	public void setCaptivitat(boolean captivitat) {
		this.captivitat = captivitat;
	}

	public float getPes() {
		return pes;
	}

	public void setPes(float pes1) {
		if(pes1>100 && pes1 < 5000) this.pes=pes1;
		else this.pes = 0;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float num) {
		if (num>1 && num<4.5) this.altura = num;
		else this.altura = 0;
	}
	
	
}
