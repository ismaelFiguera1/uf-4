package cat.almata.a014;

public class Camio {
	private String matricula;

	public Camio(String matricula) {
		this.setMatricula(matricula);
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	
}
