package cat.almata.a011;

public class Persona {
	private String nom;
	private String dni;
	
	
	public Persona(String nom, String dni) {
		setNom(nom);
		setDni(dni);
	}
	
	
	public Persona() {}


	public Persona(String dni) {
		this.dni = dni;
	}


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDni() {
		this.dni=dni.toUpperCase();
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	

	
	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", dni=" + dni + "]";
	}


	public void correr() {
		System.out.println("Estic corrent a 5km hora.");
	}
	
}
