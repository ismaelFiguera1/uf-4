package cat.almata.a016;

public class PlacaBase {
	XipSet xip = new XipSet();
	//	Constants
	public static final String MISS_WARNING_DISPOSITIU_INEXISTENT="No pots conectar un dispositiu que no es compatible";
	public static final String MISS_INFO_INSTALAT_CORRECTAMENT="Dispositiu instalat correctament";
	
	/**
	 * @param micro
	 * @return
	 * 1-> dispositiu no compatible
	 */
	public int afegirModul(Microprocesssador micro) {
		int error =2;
		//	codi que comprova la correctesa del micro amb el xipset
		if(!xip.frequenciaRam.equals(micro.getFrequencia())) {
			error = 1;
		}
		//mes comprovacions...
		return error;
	}
	
	public static void infoerror(int error) {
		switch(error) {
		case 1:
			System.out.println(MISS_WARNING_DISPOSITIU_INEXISTENT);break;
		case 2:
			System.out.println(MISS_INFO_INSTALAT_CORRECTAMENT);break;
		}
	}
}
