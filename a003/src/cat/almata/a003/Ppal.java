package cat.almata.a003;

public class Ppal {

	public static void main(String[] args) {
		Computadora c1, c2;
		
		c1=new Computadora(32, "Intel Core f4", 01);
		System.out.println("Computadora 1\n");
		System.out.println("Ram:\t"+c1.getRam()+"\nProcessador:\t"+c1.getProcessador()+
				"\nId:\t"+c1.getId());
		c1.apagar();
		
		
		c2=new Computadora(46, "Intel Core a75", 02);
		System.out.println("\n\nComputadora 2\n");
		System.out.println("Ram:\t"+c2.getRam()+"\nProcessador:\t"+c2.getProcessador()+
				"\nId:\t"+c2.getId());
		c2.reiniciar();
	}

}
