package cat.almata.a002;

public class Ppal {

	public static void main(String[] args) {
		Persona p1,p2;
		p1=new Persona();
		p1.setNom("Joan");
		p1.setCognom("Jimenez Santos");
		p1.setEdat(6);			//	Mes petit de 120
		p1.setAltura(73.7f);	//	Es te que ficar en centimetres	//	Mes petit que 235 =2.35 metres
		p1.setEsCasat(true);
		
		p1=new Persona("Leo Messi");
		

		p2=new Persona();
		p2.setNom("Maria");
		p2.setCognom("Soles Santos");
		p2.setEdat(42);
		p2.setAltura(183.02f);	//	Es te que ficar en centimetres
		p2.setEsCasat(true);
		
		p2=new Persona("Alex", 10, 183.63f);
		System.out.println("\nNom:\t"+p2.getNom() +"\nEdat:\t"+p2.getEdat()+"\nAltura:\t"+p2.getAltura()+"\n");
		
		//	Imprimir persones
		System.out.println("PERSONA 1\n");
		
		System.out.println("El nom de la persona 1 es: "+p1.getNom());
		System.out.println("La edat es: "+p1.getEdat());
		System.out.println("La alçada es: "+p1.getAltura());
		if(p1.isEsCasat()==true) System.out.println("Esta casat");
		else System.out.println("No esta casat");
		System.out.println("Posem a menjar la persona 1");
		p1.menjar();
		
		
		System.out.println("\n\nPERSONA 2\n");
		
		System.out.println("El nom de la persona 2 es: "+p2.getNom());
		System.out.println("La edat es: "+p2.getEdat());
		System.out.println("La alçada es: "+p2.getAltura());
		if(p2.isEsCasat()==true) System.out.println("Esta casada");
		else System.out.println("No esta casada");
		System.out.println("Posem a menjar la persona 2");
		p2.menjar();
		

		Elefant e1, e2;
		e1 = new Elefant();
		
		e1.setEdat(52);			//	El maxim es 90
		e1.setGenere("Femeni");
		e1.setCaptivitat(true);
		e1.setPes(4326.235f);			//	Es te que ficar en quilos
		e1.setAltura(4.235f);	//	Te que ser mes gran de 1 i mes petit de 4.5
		
		
		e2 = new Elefant();
		
		e2.setEdat(36);			
		e2.setGenere("Masculi");
		e2.setCaptivitat(false);
		e2.setPes(3000.230f);
		e2.setAltura(3.689f);
		
		//	Imprimir elefants
		
		System.out.println("\n\n\nElefant 1:\n");
		System.out.println("Edat: "+e1.getEdat());
		System.out.println("Genere: "+e1.getGenere());
		if(e1.isCaptivitat()==true) System.out.println("Captivitat: Si");
		else System.out.println("Captivitat: No");
		System.out.println("Pes: "+e1.getPes());
		System.out.println("Altura: "+e1.getAltura());
		System.out.println("Ficar el elefant 1 a beurer:");
		e1.beurer();
		
		System.out.println("\n\nElefant 2:\n");
		System.out.println("Edat: "+e2.getEdat());
		System.out.println("Genere: "+e2.getGenere());
		if(e2.isCaptivitat()==true) System.out.println("Captivitat: Si");
		else System.out.println("Captivitat: No");
		System.out.println("Pes: "+e2.getPes());
		System.out.println("Altura: "+e2.getAltura());
		System.out.println("Ficar el elefant 2 a caminar:");
		e2.caminar();
		
	}

}
