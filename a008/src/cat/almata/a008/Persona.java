package cat.almata.a008;

public class Persona {
	private String nom;
	private String dni;
	
	
	public Persona(String nom, String dni) {
		setNom(nom);
		setDni(dni);
	}
	
	
	public Persona() {}


	public Persona(String dni) {
		this.dni = dni;
	}


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDni() {
		this.dni=dni.toUpperCase();
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	
}
