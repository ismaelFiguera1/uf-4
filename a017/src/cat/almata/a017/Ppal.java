package cat.almata.a017;



public class Ppal {
	public static void main(String[] args) {
		Alumne a1 = new Alumne("23m", "Joshua");
		Alumne a2 = new Alumne("23a", "Alex");
		Curs c1 = new Curs(1,"Programacio");
		Curs c2 = new Curs(2,"Base de dades");
		
		a1.inscriurerCurs(c1);
		a2.inscriurerCurs(c1);
		a1.inscriurerCurs(c2);
		
		System.out.println("Curs 1");
		
		for(Alumne a : c1.rescatarAlumne()) {
			System.out.println(a.getNom());
			System.out.println(a.getDni());
		}
		
		System.out.println("Curs 2");
		
		for(Alumne a : c2.rescatarAlumne()) {
			System.out.println(a.getNom());
			System.out.println(a.getDni());
		}
		
		
		System.out.println("Alumne 1");
		for(Curs c : a1.rescatarCurs()) {
			System.out.println(c.getId());
			System.out.println(c.getNom());
		}
		
		System.out.println("Alumne 2");
		for(Curs c : a2.rescatarCurs()) {
			System.out.println(c.getId());
			System.out.println(c.getNom());
		}
		
	}
}
