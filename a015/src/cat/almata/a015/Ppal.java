package cat.almata.a015;

public class Ppal {
	public static void main(String[] args) {
		Refresc r1 = new Refresc("Dolç", "a102");
		Refresc r2 = new Refresc("Cocacola", "a132");
		Contenidor c1 = new Contenidor("Plastic");
		Contenidor c2 = new Contenidor("Vidre");
		Caixa caixa1 = new Caixa("Fusta", "0213ad");
		
		c1.afegirRefresc(r1);
		c2.afegirRefresc(r2);
	//	System.out.println(c1.obtenirRefresc().getSabor());
		
		caixa1.addBotella(c1);
		caixa1.addBotella(c2);
		
		for(Contenidor c : caixa1.getCaixa()) {
			System.out.println(c.obtenirRefresc().getSabor());
		}
		
		
		
	}
}
