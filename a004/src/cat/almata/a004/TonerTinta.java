package cat.almata.a004;

public class TonerTinta {
	private String color;
	private  int pagines;
	private String model;
	private String id;
	
	
	
	public TonerTinta() {}
	
	public TonerTinta(String id) {
		setId(id);
	}
	
	public TonerTinta(String color, int pagines, String model, String id) {
		setColor(color);
		setPagines(pagines);
		setModel(model);
		setId(id);
	}
	
	
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color=color;
	}
	
	public int getPagines() {
		return pagines;
	}
	
	public void setPagines(int pagines) {
		if(pagines<5000) this.pagines=pagines;
		else this.pagines=0;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
