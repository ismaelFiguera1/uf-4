package cat.almata.a017;

import java.util.ArrayList;

public class Alumne {
	private String dni;
	private String nom;
	
	public Alumne(String dni, String nom) {
		super();
		setDni(dni);
		setNom(nom);
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	//	Atributs relacio
	ArrayList<Curs> cursos= new ArrayList<Curs>();
	
	public void inscriurerCurs(Curs curs) {
		cursos.add(curs);
		// Si el alumne no existeix al curs
		curs.afegirAlumne(this);
	}
	
	public ArrayList<Curs> rescatarCurs(){
		return cursos;
	}
}
