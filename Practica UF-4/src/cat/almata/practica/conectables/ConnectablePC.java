package cat.almata.practica.conectables;

import cat.almata.practica.PlacaBase.XipSet;

public interface ConnectablePC {
	public abstract boolean isConnectable(XipSet x, Object obj);
}
