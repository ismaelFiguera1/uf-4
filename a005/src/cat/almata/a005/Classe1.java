package cat.almata.a005;

public class Classe1 {

	private String nom="Pepe";
	public String cognoms = "Gimenez Mata";
	protected int edat = 12;
	double temperatura=23.365;
	
	
	public Classe1() {
		System.out.println(nom.length());
		System.out.println(nom.charAt(2));
		System.out.println(nom.equals("Pepe"));
		System.out.println(nom.toUpperCase());
		System.out.println(nom.toLowerCase());
		edat=32;
		Integer edat2 = new Integer(edat);  //	Transformo el atribut edat a classe edat2	//	Versio antiga
		Integer edat3 = Integer.valueOf(edat); //	Versio moderna 
		String edat2Cadena=edat2.toString();	//	edat2.toString();	Me retorna una cadena que almaçeno a edat2Cadena
		System.out.println(edat2Cadena);
	}
	
	
	
	public static void main(String[] args) {
		Classe1 c = new Classe1();
		
	}
	
}
