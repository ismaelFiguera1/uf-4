package cat.almata.a019;

public class Ppal {
	public static void main(String[] args) {
		Persona p1 = new Persona("Alex", "1");
		Persona p2 = new Persona("Lluis", "2");
		Persona p3 = new Persona("Joan", "3");
		Persona p4 = new Persona("Amalia", "4");
		Persona p5 = new Persona("Miquel", "5");
		
		//	La Amalia i el Miquel son amics del Alex
		p1.afegirAmic(p4);
		p1.afegirAmic(p5);
		
		// Mostra els amic del Alex
		for(Persona p: p1.returnAmics()) {
			System.out.println(p.getNom());
		}
	}
}
