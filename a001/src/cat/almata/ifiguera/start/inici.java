package cat.almata.ifiguera.start;

public class inici {
	
	public static void main(String[] args) {
		//	Comentari linea
		
		/*
		 * Comentari
		 * de
		 * mes
		 * de
		 * una
		 * linea
		 */
		
		int edat1=4, edat2=55;
		int edats[]= new int [10];
		long distancia;
		float temperatura;
		double diners;
		boolean esCasat=true;
		
		
		if(edat1>edat2) {
			System.out.println("Edat1 es mes gran "+edat1);
		}else {
			System.out.println("Edat2 es mes gran "+edat2);
		}
		
		//	Iteracions
		for(int j=1;j<edat1;j++) {
			System.out.println(j);
		}
		
		int min=1;
		
		while(min<edat2) {
			min++;
		}
		
		int j=0;
		do {
			System.out.println(j);
			j++;
		} while (j<10);
		
	}

}
