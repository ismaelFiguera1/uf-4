package cat.almata.a018;

public class Memoria {
	private String frequencie;
	private String id;
	
	
	public Memoria(String frequencie, String id) {
		super();
		this.frequencie = frequencie;
		this.id = id;
	}
	
	public String getFrequencie() {
		return frequencie;
	}
	
	public void setFrequencie(String frequencie) {
		this.frequencie = frequencie;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
}
