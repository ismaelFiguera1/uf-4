package cat.almata.a010;

public abstract class Ocell {
	private String nom;
	
	public abstract void cantar();

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
