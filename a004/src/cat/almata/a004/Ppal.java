package cat.almata.a004;

public class Ppal {

	public static void main(String[] args) {
		Impresora i3 = null;
		TonerTinta toner1 = null, toner2=null;


		
		i3 = new Impresora("Laserjet 200", "HP", 3);
		
		toner1 = new TonerTinta();
		toner1.setColor("Negre");
		toner1.setId("01-a");
		toner1.setPagines(1200);
		toner1.setModel("912XL");
		
		toner2 = new TonerTinta();
		toner2.setColor("Blau");
		toner2.setId("01-b");
		toner2.setPagines(1200);
		toner2.setModel("910XL");
		
		//	Fiquem el toner1 a la impresora 3
		i3.posarToner(toner1);
		i3.posarToner(toner2);
		
		System.out.println("La impresora te el toner "+i3.getTonerTinta().getId()+", "
		+i3.getTonerTinta().getColor());
	}

}
