package cat.almata.a005;

import java.util.Scanner;


public class Ppal {

	public static void main(String[] args) {
		//	Declarar objecte per a entrar dades desde teclat
		Scanner entradaTeclat=new Scanner(System.in);
		System.out.println("Fica el nom:\t");
		String nom = entradaTeclat.next();	//	Entra espai fins un blanc (espai)
		entradaTeclat.nextLine();	//	Buida buffer
		System.out.println("El nom introduit es: "+nom);
		
		System.out.println("Fica els cognoms:\t");
		String cognoms = entradaTeclat.nextLine();	//	Permet espais
		System.out.println("Els cognoms son: "+cognoms);
		
		System.out.println("Entra la edat");
		int edat = entradaTeclat.nextInt();
		System.out.println("La edat es: "+edat);
		
		System.out.println("Entra la temperatura");
		double temperatura = entradaTeclat.nextDouble();
		System.out.println("La temperatura es "+temperatura);

	}

}
