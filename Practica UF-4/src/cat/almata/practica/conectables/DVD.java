package cat.almata.practica.conectables;

import cat.almata.practica.PlacaBase.XipSet;

public class DVD implements ConnectablePC {

	private String velocitatEscriptura;
	private String velocitatLectura;
	private String connector;
	private String id;
	
	
	
	
	public DVD(String velocitatEscriptura, String velocitatLectura, String connector, String id) {
		super();
		this.velocitatEscriptura = velocitatEscriptura;
		this.velocitatLectura = velocitatLectura;
		this.connector = connector;
		this.id = id;
	}
	
	




	public String getVelocitatEscriptura() {
		return velocitatEscriptura;
	}






	public void setVelocitatEscriptura(String velocitatEscriptura) {
		this.velocitatEscriptura = velocitatEscriptura;
	}






	public String getVelocitatLectura() {
		return velocitatLectura;
	}






	public void setVelocitatLectura(String velocitatLectura) {
		this.velocitatLectura = velocitatLectura;
	}






	public String getConnector() {
		return connector;
	}






	public void setConnector(String connector) {
		this.connector = connector;
	}






	public String getId() {
		return id;
	}






	public void setId(String id) {
		this.id = id;
	}


	




	@Override
	public String toString() {
		return "DVD [velocitatEscriptura=" + velocitatEscriptura + ", velocitatLectura=" + velocitatLectura
				+ ", connector=" + connector + ", id=" + id + "]";
	}






	@Override
	public boolean isConnectable(XipSet x, Object obj) {
		boolean ser;
		DVD d1 = (DVD) obj;
		if(d1.getConnector().equals(x.getConnectorDVD())) {
			ser = true;
		}else {
			ser = false;
		}
		return ser;
	}

}
