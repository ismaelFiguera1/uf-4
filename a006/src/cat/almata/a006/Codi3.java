package cat.almata.a006;

import java.util.Scanner;

public class Codi3 {
	private String text;
	
	public Codi3(){
		setText();
		getText();
		System.out.println("El text te "+numeroCaracters() +" caracters.");
		System.out.println("El text te "+contarVocals() +" vocals.");
		System.out.println("El text te "+contarConsonants() +" consonants");
	}
	
	public String getText() {
		System.out.println("El text es:\t" +text);
		return text;
	}

	public void setText() {
		Scanner entradaTeclat=new Scanner(System.in);
		System.out.println("Fica el text:\t");
		text = entradaTeclat.nextLine();
		entradaTeclat.close();
	}
	
	public int numeroCaracters() {
		int i = text.length();
		return i;
	}
	
	public int contarVocals() {
		int contador=0;
		text.toLowerCase();
		for(int i=0; i<text.length(); i++) {
			if(text.charAt(i)=='a' || text.charAt(i)=='e' || text.charAt(i)=='i' || text.charAt(i)=='o' || text.charAt(i)=='u') contador++;
		}
		return contador;
	}
	
	public int contarConsonants() {
		int contador = 0, a;
		for(int i=0; i<text.length(); i++) {
			if(text.charAt(i)==' ') contador++;
		}
		a=text.length()-(contador + contarVocals());
		return a;
	}
	
}
