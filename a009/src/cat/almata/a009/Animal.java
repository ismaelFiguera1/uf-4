package cat.almata.a009;

public class Animal {
	private int edat;
	private double pes;
	private String nom;
	private int id;
	private boolean esViu;
	
	public Animal() {
		super();
	}

	public Animal(int id) {
		super();
		setId(id);
	}

	public Animal(int edat, double pes, String nom, int id, boolean esViu) {
		super();
		setEdat(edat);
		setPes(pes);
		setNom(nom);
		setId(id);
		setEsViu(esViu);
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		if(edat>150) this.edat=0;
		else this.edat = edat;
	}

	public double getPes() {
		return pes;
	}

	public void setPes(double pes) {
		if(pes<10000) this.pes = pes;
		else this.pes = 0;
		
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isEsViu() {
		return esViu;
	}

	public void setEsViu(boolean esViu) {
		this.esViu = esViu;
	}

	@Override
	public String toString() {
		return "Animal [edat=" + edat + ", pes=" + pes + ", nom=" + nom + ", id=" + id + ", esViu=" + esViu + "]";
	}

	public void menjar() {
		System.out.println("Esta menjant");
	}
	
	
	
	
}
