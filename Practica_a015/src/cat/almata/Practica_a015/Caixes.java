package cat.almata.Practica_a015;

import java.util.ArrayList;

public class Caixes {
	private String material;
	private int id;
	
	//	Referencies de cardinalitats
	ArrayList<Botella> botelles = new ArrayList<Botella>();
	
	public void afegirBotellesACaixa(Botella b) {
		botelles.add(b);
	}
	
	/*
	 * Aixo serveix per a fer operacions amb les dades de la caixa,
	 * com agafar la botella numero x i veurer el liquid que te...
	 */
	public ArrayList<Botella> getCaixa() {
		return botelles;
	}
	
	
	
	//	Coses classe
	public Caixes(String material, int id) {
		super();
		this.material = material;
		this.id = id;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
