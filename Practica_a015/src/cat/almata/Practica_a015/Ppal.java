package cat.almata.Practica_a015;

public class Ppal {
	public static void main(String[] args) {
		Liquid l1 = new Liquid("Cocacola");
		Liquid l2 = new Liquid("Fanta");
		Botella b1 = new Botella("Vidre", 1);
		Botella b2 = new Botella("Plastic", 2);
		b1.afegirLiquidABotella(l1);
		b2.afegirLiquidABotella(l2);
		
		
		
		Caixes c1 = new Caixes("Fusta", 1);
		
		c1.afegirBotellesACaixa(b1);
		c1.afegirBotellesACaixa(b2);
		
		for(Botella b : c1.getCaixa()) {
			System.out.println(b.obtenirBotellaPlena().getNom());
		}
		
		
	}
}
