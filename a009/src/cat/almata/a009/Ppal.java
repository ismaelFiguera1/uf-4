package cat.almata.a009;

public class Ppal {

	public static void main(String[] args) {
		Animal a1 = new Animal(56, 654.236, "Simba", 01, true);
		System.out.println(a1);
		Elefant e1 = new Elefant(82, 7231.065, "Dumbo", 02, true, 143.652);
		System.out.println(e1);
		Animal a2 = new Animal(03);
		a2.setNom("Mani");
		a2.setEdat(94);
		Elefant e2 = new Elefant(a2, 102.36);
		System.out.println(e2);

	}

}
