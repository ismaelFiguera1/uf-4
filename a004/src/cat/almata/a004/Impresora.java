package cat.almata.a004;

public class Impresora {
	private String model;
	private String marca;
	private int id;
	
	//	Atributs fruit de la relacio
	private TonerTinta toner = null;
	
	//	Constructors
	public Impresora() {
		
	}

	public Impresora(String model, String marca, int id) {
		setModel(model);
		setMarca(marca);
		setId(id);
	}
	
	public Impresora(int id) {
		setId(id);
	}
	
	
	//	Metodes de negoci
	
	
	//	Fica el toner que li arriba 
	public boolean posarToner(TonerTinta toner) {
		boolean correcte = true;
		if(!hiHaToner() && toner != null) this.toner = toner;
		else System.out.println("Ja hi ha toner a l'impresora");
		return correcte;
	}
	
	public boolean hiHaToner() {
		boolean haver=true;
		if(toner == null) haver=false;
		return haver;
	}
	
	public TonerTinta getTonerTinta() {
		return toner;
	}
	
	
	//	Getters i setters

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
