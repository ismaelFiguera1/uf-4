package cat.almata.a019;

import java.util.ArrayList;

public class Persona {
	private String nom;
	private String dni;
	
	//	Atribut fruit de la relacio
	private ArrayList<Persona>amics = new ArrayList<Persona>();
	
	public void afegirAmic(Persona amic) {
		amics.add(amic);
	}
	
	public boolean isEmpty() {
		return amics.isEmpty();
	}
	
	public ArrayList<Persona> returnAmics(){
		return amics;
	}
	
	public Persona(String nom, String dni) {
		super();
		this.nom = nom;
		this.dni = dni;
	}
	
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	
}
