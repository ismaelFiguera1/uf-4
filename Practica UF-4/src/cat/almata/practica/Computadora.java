package cat.almata.practica;

import cat.almata.practica.PlacaBase.XipSet;
import cat.almata.practica.conectables.DVD;
import cat.almata.practica.conectables.DiscDur;
import cat.almata.practica.conectables.FontAlimentacio;
import cat.almata.practica.conectables.Monitor;
import cat.almata.practica.conectables.Ratoli;
import cat.almata.practica.conectables.Teclat;

public class Computadora {
	private String id;
	private boolean tapaOberta=false;
	private int badies_5_1_4;
	private int badies_3_1_2;
	
	
	public static final String TAPA_TANCADA="La tapa esta tancada, obra-la o pots encendre el ordenador.";
	public static final String NO_PLACABASE="No hi ha cap placa base instalada, la pots instalar.";
	public static final String SI_PLACABASE="Ja hi ha una placa base instalada.";
	
	public static final String SI_DISCDUR="Ja hi ha un Discdur connectat.";
	public static final String NO_DISCDUR="El discdur sa connectat correctament";
	
	public static final String SI_FONTALIMENTACIO="Ja hi ha una Font d'alimentacio connectada.";
	public static final String NO_FONTALIMENTACIO="La font d'alimentacio sa connectat correctament";
	
	public static final String SI_MONITOR="Ja hi ha un monitor connectat.";
	public static final String NO_MONITOR="El monitor sa connectat correctament";
	
	public static final String SI_DVD="Ja hi ha un dvd connectat.";
	public static final String NO_DVD="El dvd sa connectat correctament";
	
	public static final String SI_TECLAT="Ja hi ha un teclat connectat.";
	public static final String NO_TECLAT="El teclat sa connectat correctament";
	
	public static final String SI_RATOLI="Ja hi ha un ratoli connectat.";
	public static final String NO_RATOLI="El ratoli sa connectat correctament";
	
	public static final String MISS_WARNING_DISCDUR_ERROR="El disc dur no es compatible, canvial.";
	public static final String MISS_WARNING_FONTALIMENTACIO_ERROR="La font d'alimentacio no es compatible, canvia-la.";
	public static final String MISS_WARNING_MONITOR_ERROR="El monitor no es compatible, canvial.";
	public static final String MISS_WARNING_DVD_ERROR="El dvd no es compatible, canvial.";
	public static final String MISS_WARNING_TECLAT_ERROR="El teclat no es compatible, canvial.";
	public static final String MISS_WARNING_RATOLI_ERROR="El ratoli no es compatible, canvial.";
	public static final String MISS_INFO_CORRECTE="Tots els connectables son correctes i compatibles.";
	
	public static final String SI_ENCENDRE="Encenent ordenador";
	public static final String NO_ENCENDRE="No es pot encendre el ordenador, hi deu haber alguna cosa que no es compatible.";
	
	
	private PlacaBase placaBase=null;
	private DiscDur disc=null;
	private FontAlimentacio font=null;
	private Monitor monitor=null;
	private DVD dvd=null;
	private Teclat teclat=null;
	private Ratoli ratoli=null;
	private XipSet xipset=null;
	private Computadora ordenador=this;
	
	public int encendreOrdenador() {
		int error;
		if(comprobarConnectors()==0 && placaBase.comprobarInstalables()==0) error=0;
		else error =1;
		return error;
	}
	
	public static void infoerrorEncendreOrdenador(int error) {
		switch(error) {
		case 0: System.out.println(SI_ENCENDRE);break;
		case 1: System.out.println(NO_ENCENDRE);break;
		}
	}
	
	public void desconectarDiscdur() {
		this.disc=null;
	}
	
	public void desconectarFontAlimentacio() {
		this.font=null;
	}
	
	public void desconectarMonitor() {
		this.monitor=null;
	}
	
	public void desconectarDVD() {
		this.dvd=null;
	}
	
	public void desconectarTeclat() {
		this.teclat=null;
	}
	
	public void desconectarRatoli() {
		this.ratoli=null;
	}
	
	public XipSet obtenirdatesXip(PlacaBase p) {
	    this.xipset = p.getXipSet();
	    return xipset;
	}

	
	public void obrirTapaComputadora() {
		tapaOberta=true;
	}
	
	public void tancarTapaComputadora() {
		tapaOberta=false;
	}
	
	public int veurerTapa_Placa() {
		int error = 0;
		
		if(tapaOberta==true) {
			if(placaBase!=null) {
				error=2;
			}
		}else error =1;
		
		return error;
	}
	
	public static void infoVeurerTapa_Placa(int error) {
		switch(error) {
		case 0:System.out.println(NO_PLACABASE);break;
		case 1:System.out.println(TAPA_TANCADA);break;
		case 2:System.out.println(SI_PLACABASE);break;
		}
	}
	
	
	public void instalarPlacaBase(PlacaBase p1) {
		this.placaBase=p1;
	}
	
	
	public int connectarDiscDur(DiscDur d1) {
		int error =0;
		
		if(disc==null) {
			this.disc=d1;
		}else {
			error = 1;
		}
		return error;
	}
	
	public static void infoerrorDiscDur(int error) {
		switch(error) {
		case 0:System.out.println(NO_DISCDUR);break;
		case 1:System.out.println(SI_DISCDUR);break;
		}
	}
	
	
	
	public int connectarFontAlimentacio(FontAlimentacio f1) {
		int error=0;
		if(font==null) {
			this.font = f1;
		}else error =1;
		
		return error;
	}
	
	public static void infoerrorFontAlimentacio(int error) {
		switch(error) {
		case 0: System.out.println(NO_FONTALIMENTACIO);break;
		case 1: System.out.println(SI_FONTALIMENTACIO);break;
		}
	}
	
	public int connectarMonitor(Monitor m1) {
		int error=0;
		
		if(monitor==null) {
			this.monitor=m1;
		}else error = 1;
		
		return error;
	}
	
	public static void infoerrorMonitor(int error) {
		switch(error) {
		case 0: System.out.println(NO_MONITOR);break;
		case 1: System.out.println(SI_MONITOR);break;
		}
	}
	
	public int connectarDVD(DVD d) {
		int error = 0;
		
		if(dvd==null) {
			this.dvd=d;
		}else error=1;
		
		return error;
	}
	
	public static void infoerrorDVD(int error) {
		switch(error) {
		case 0: System.out.println(NO_DVD);break;
		case 1: System.out.println(SI_DVD);break;
		}
	}
	
	public int connectarTeclat(Teclat t) {
		int error = 0;
		
		if(teclat==null) {
			this.teclat=t;
		}else error=1;
		
		return error;
	}
	
	public static void infoerrorTeclat(int error) {
		switch(error) {
		case 0: System.out.println(NO_TECLAT);break;
		case 1: System.out.println(SI_TECLAT);break;
		}
	}
	
	public int connectarRatoli(Ratoli r) {
		int error=0;
		
		if(ratoli==null) {
			this.ratoli=r;
		}else error=1;
		
		return error;
	}
	
	public static void infoerrorRatoli(int error) {
		switch(error) {
		case 0: System.out.println(NO_RATOLI);break;
		case 1: System.out.println(SI_RATOLI);break;
		}
	}
	
	
	//	Fer comprovacio de que sigui compatible
	public int comprobarConnectors() {
		int error;
		xipset=ordenador.obtenirdatesXip(placaBase);
		if(disc.isConnectable(xipset, disc)==true) {
			if(dvd.isConnectable(xipset, dvd)==true) {
				if(font.isConnectable(xipset, font)==true) {
					if(monitor.isConnectable(xipset, monitor)==true) {
						if(ratoli.isConnectable(xipset, ratoli)==true) {
							if(teclat.isConnectable(xipset, teclat)==true) error=0;
							else error = 6;
						}else {
							error=5;
						}
					}else error=4;
				}else {
					error=3;
				}
			}else {
				error = 2;
			}
		}else{
			error = 1;
		}
		return error;
	}
	
	public static void infoerrorComprobar(int error) {
		switch (error) {
		case 0: System.out.println(MISS_INFO_CORRECTE);break;
		case 1: System.out.println(MISS_WARNING_DISCDUR_ERROR);break;
		case 2: System.out.println(MISS_WARNING_DVD_ERROR);break;
		case 3: System.out.println(MISS_WARNING_FONTALIMENTACIO_ERROR);break;
		case 4: System.out.println(MISS_WARNING_MONITOR_ERROR);break;
		case 5: System.out.println(MISS_WARNING_RATOLI_ERROR);break;
		case 6: System.out.println(MISS_WARNING_TECLAT_ERROR);break;
		}
	}
	

	public Computadora(String id, int badies_5_1_4, int badies_3_1_2) {
		super();
		setId(id);
		setBadies_5_1_4(badies_5_1_4);
		setBadies_3_1_2(badies_3_1_2);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getBadies_5_1_4() {
		return badies_5_1_4;
	}

	public void setBadies_5_1_4(int badies_5_1_4) {
		this.badies_5_1_4 = badies_5_1_4;
	}

	public int getBadies_3_1_2() {
		return badies_3_1_2;
	}

	public void setBadies_3_1_2(int badies_3_1_2) {
		this.badies_3_1_2 = badies_3_1_2;
	}
	
	
	
}
