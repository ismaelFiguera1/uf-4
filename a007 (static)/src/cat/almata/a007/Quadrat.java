package cat.almata.a007;

public class Quadrat {
	private double costat;
	private String color;
	
	public Quadrat() {}
	
	public Quadrat(Quadrat q) {
		
	}
	
	public Quadrat(double costat, String color) {
		this(costat);
		setColor(color);
	}
	
	public Quadrat(double costat) {
		setCostat(costat);
	}
	
	
	
	

	public double getCostat() {
		return costat;
	}
	
	public void setCostat(double costat) {
		this.costat=costat;
	}
	
	public double area() {
		return costat*costat;
	}
	
	public static double area(double costat) {
		return costat*costat;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public Quadrat clonarQuadrat(Quadrat q) {
		Quadrat c = new Quadrat();
		c.setColor(q.getColor());
		/*....*/
		this.clonarQuadrat(this);
		
		return c;
		
	}
	
	
}
