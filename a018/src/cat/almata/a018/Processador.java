package cat.almata.a018;

public class Processador {
	private String socol;
	private String id;
	
	
	public Processador(String socol, String id) {
		super();
		this.socol = socol;
		this.id = id;
	}
	
	public String getSocol() {
		return socol;
	}
	
	public void setSocol(String socol) {
		this.socol = socol;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	
}
