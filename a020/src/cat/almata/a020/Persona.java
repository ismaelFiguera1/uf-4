package cat.almata.a020;

import java.util.ArrayList;
import java.util.Objects;

public class Persona {
	private String nom;
	private String dni;
	

	public Persona(String nom, String dni) {
		super();
		setNom(nom);
		setDni(dni);
	}
	
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}


	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		return Objects.equals(dni, other.dni);
	}

/*
	@Override
	public boolean equals(Object obj) {
		boolean iguals=true;
		if(obj==null) iguals=false;
		if (!(obj instanceof Persona)) iguals = false;
		else {
			Persona p = (Persona) obj;
			if(p.getDni().equals(this.getDni())) iguals=true;
			else iguals=false;
		}
		return iguals;
	}
	*/
	
}
