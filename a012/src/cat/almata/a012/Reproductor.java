package cat.almata.a012;

public class Reproductor {
	private String material;
	
	

	public Reproductor(String material) {
		super();
		setMaterial(material);
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}
	
	public void play() {
		System.out.println("No se com d'onar-li al PLAY.");
	}

	@Override
	public String toString() {
		return "Reproductor [material=" + material + "]";
	}


	
	
}
