package cat.almata.Practica_a015;

public class Botella {
	private String material;
	private int id;
	
	//	Relacio amb Liquid
	private Liquid l1=null;
	
	
	public void afegirLiquidABotella(Liquid l) {
		l1 = l;
	}
	
	public Liquid obtenirBotellaPlena() {
		return l1;
	}
	
	
	
	
	
	//	Coses de la classe Botella
	
	public Botella(String material, int id) {
		super();
		this.material = material;
		this.id = id;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
