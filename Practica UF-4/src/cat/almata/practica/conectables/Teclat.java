package cat.almata.practica.conectables;

import cat.almata.practica.PlacaBase.XipSet;

public class Teclat implements ConnectablePC {

	private String marca;
	private String tecnologia;
	private String id;
	private String connector;
	
	
	
	
	public Teclat(String marca, String tecnologia, String id, String connector) {
		super();
		setMarca(marca);
		setTecnologia(tecnologia);
		setId(id);
		setConnector(connector);
	}


	


	public String getMarca() {
		return marca;
	}





	public void setMarca(String marca) {
		this.marca = marca;
	}





	public String getTecnologia() {
		return tecnologia;
	}





	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}





	public String getId() {
		return id;
	}





	public void setId(String id) {
		this.id = id;
	}


	public String getConnector() {
		return connector;
	}





	public void setConnector(String connector) {
		this.connector = connector;
	}
	


	@Override
	public String toString() {
		return "Teclat [marca=" + marca + ", tecnologia=" + tecnologia + ", id=" + id + "]";
	}





	@Override
	public boolean isConnectable(XipSet x, Object obj) {
		boolean ser;
		Teclat t1 = (Teclat) obj;
		if(t1.getConnector().equals(x.getConnectorTeclat())) {
			ser = true;
		}else {
			ser = false;
		}
		return ser;
	}





	

}
