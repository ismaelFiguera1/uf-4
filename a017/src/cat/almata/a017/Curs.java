package cat.almata.a017;

import java.util.ArrayList;



public class Curs {
	private int id;
	private String nom;

	public Curs(int id, String nom) {
		super();
		setId(id);
		setNom(nom);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	
	//	Atributs relacio
	
	ArrayList<Alumne> alumnes= new ArrayList<Alumne>();
	
	public void afegirAlumne(Alumne alumne) {
		alumnes.add(alumne);
	}
	
	public ArrayList<Alumne> rescatarAlumne(){
		return alumnes;
	}


	
}
