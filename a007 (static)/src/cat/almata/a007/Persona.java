package cat.almata.a007;

public class Persona {
	private String nom;
	private static int contadorPersones=0;
	
	public Persona() {
		contadorPersones++;
	}
	
	public static int getContadorPersones() {
		return contadorPersones;
	}
}
