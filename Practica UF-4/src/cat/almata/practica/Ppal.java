package cat.almata.practica;

import cat.almata.practica.PlacaBase.XipSet;
import cat.almata.practica.conectables.DVD;
import cat.almata.practica.conectables.DiscDur;
import cat.almata.practica.conectables.FontAlimentacio;
import cat.almata.practica.conectables.Monitor;
import cat.almata.practica.conectables.Ratoli;
import cat.almata.practica.conectables.Teclat;
import cat.almata.practica.instalables.Memoria;
import cat.almata.practica.instalables.Microprocessador;
import cat.almata.practica.instalables.Tarjeta_Grafica;

public class Ppal {

	public static void main(String[] args) {
		PlacaBase placaBase1 = new PlacaBase();
		//Hem creat la PlacaBase
		//XipSet x1 = PlacaBase.getXipSet();
		//ara fem que x1 sigui el XipSet de la PlacaBase
		//Creem el microProcessador
		Microprocessador microprocessador = new Microprocessador(30.2, "LGA1366", "Intel Core i7", "a01");
		
		
		//	Creem la targeta Grafica
		Tarjeta_Grafica tarja = new Tarjeta_Grafica("AGP", "Sony", "0231", "2", "a01");
		
		//	Creem la memoria
		Memoria memoria = new Memoria(1333, "DDR4", "a01");
		
		
		//	Ara fem la comprovacio de que sigui correcte
		
		//En la seguent linea sinstala i es comprova si hi ha un instalable ja ficat
		PlacaBase.infoMicroprocessador(placaBase1.instalarMicroprocessador(microprocessador));
		PlacaBase.infoTarja_Grafica(placaBase1.instalarTargeta_Grafica(tarja));
		PlacaBase.infoMemoria(placaBase1.instalarMemoria(memoria));
		
		PlacaBase.infoerrorComprobar(placaBase1.comprobarInstalables());
		
		//	Creem una computadora
		Computadora ordenador = new Computadora("a001", 4, 1);
		
		//	fem diverser comprobacions abans de instalar la placa base
		
		Computadora.infoVeurerTapa_Placa(ordenador.veurerTapa_Placa());
		ordenador.obrirTapaComputadora();
		Computadora.infoVeurerTapa_Placa(ordenador.veurerTapa_Placa());
		ordenador.instalarPlacaBase(placaBase1);
		Computadora.infoVeurerTapa_Placa(ordenador.veurerTapa_Placa());
		
		//	Ara que ja tenim instalada la placa base, creem els conectables i els connectem a la computadora
		DiscDur disc = new DiscDur(30.23, "Sata", "a01");
		FontAlimentacio font = new FontAlimentacio(3, "32 pins", "a01");
		Monitor monitor = new Monitor(2.36, 0.32, "HDMI", "01");
		DVD dvd=new DVD("54g", "56l", "Sata", "a01");
		Teclat teclat = new Teclat("sony", "paralel", "a01", "PS2");
		Ratoli ratoli = new Ratoli("sony", "USB", "a01");
		Computadora.infoerrorDiscDur(ordenador.connectarDiscDur(disc));  
		Computadora.infoerrorFontAlimentacio(ordenador.connectarFontAlimentacio(font));
		Computadora.infoerrorMonitor(ordenador.connectarMonitor(monitor));
		Computadora.infoerrorDVD(ordenador.connectarDVD(dvd));
		Computadora.infoerrorTeclat(ordenador.connectarTeclat(teclat));
		Computadora.infoerrorRatoli(ordenador.connectarRatoli(ratoli));
		
		
		// Ara comprobem que siguin compatibles
		Computadora.infoerrorComprobar(ordenador.comprobarConnectors());
		ordenador.tancarTapaComputadora();
		Computadora.infoVeurerTapa_Placa(ordenador.veurerTapa_Placa());
		Computadora.infoerrorEncendreOrdenador(ordenador.encendreOrdenador());
		
		

	}
	
	

}
