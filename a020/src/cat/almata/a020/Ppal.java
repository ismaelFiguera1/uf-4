package cat.almata.a020;

public class Ppal {

	public static void main(String[] args) {
		Persona p1 = new Persona("Pepe", "a01");
		Persona p2 = new Persona("Lluis", "a01");
		
		if(p1==p2) System.out.println("Son iguals");
		else System.out.println("No son iguals");
		
		if(p1.equals(p2)) {
			System.out.println("Son iguals");
		}else {
			System.out.println("No son iguals");
		}

	}

}
