package cat.almata.a015;

public class Refresc {
	private String sabor;
	private String id;
	
	public Refresc (String sabor, String id) {
		this.setSabor(sabor);
		this.setId(id);
	}

	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
