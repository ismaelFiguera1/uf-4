package cat.almata.a012;

public class Dvd extends Reproductor {
	private String support;

	

	public Dvd(String material, String support) {
		super(material);
		setSupport(support);
	}

	public String getSupport() {
		return support;
	}

	public void setSupport(String support) {
		this.support = support;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Li d'ono al play amb el mando de la Tele.");
	}

	@Override
	public String toString() {
		return "Dvd [support=" + support + ", toString()=" + super.toString() + "]";
	}
	
	
	
	
}
