package cat.almata.a012;

public class Vhs extends Reproductor {
	private String color;

	public Vhs(String material, String color) {
		super(material);
		setColor(color);
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Vhs [color=" + color + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Li d'ono play clickant un boto.");
	}

	
	
}
