package cat.almata.a013;

public class Ppal {

	public static void main(String[] args) {
		
		Reproductor r2 = new Dvd("Plastic", "Media");
		Reproductor r3 = new Vhs("Acer", "Roig"); 
	
		
		descripcioReproductors(r2);
		playReproductors(r2);
		nextReproductors(r2);
		pauseReproductors(r2);
		previousReproductors(r2);
		stopReproductors(r2);
		
		System.out.println("\n\n\n");
		
		descripcioReproductors(r3);
		playReproductors(r3);
		nextReproductors(r3);
		pauseReproductors(r3);
		previousReproductors(r3);
		stopReproductors(r3);
		
		
		
	}
	
	public static void playReproductors(Reproductor r) {
		r.play();
	}
	
	public static void stopReproductors(Reproductor r) {
		r.stop();
	}
	
	public static void nextReproductors(Reproductor r) {
		r.next();
	}
	
	public static void pauseReproductors(Reproductor r) {
		r.pause();
	}
	
	public static void previousReproductors(Reproductor r) {
		r.previous();
	}
	
	public static void descripcioReproductors(Reproductor r) {
		System.out.println(r);
	}
	
	

}
