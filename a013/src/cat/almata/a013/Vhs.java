package cat.almata.a013;

public class Vhs extends Reproductor {
	private String color;

	public Vhs(String material, String color) {
		super(material);
		setColor(color);
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Vhs [color=" + color + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Li d'ono play clickant un boto.");
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		System.out.println("Parar VHS");
	}

	@Override
	public void next() {
		// TODO Auto-generated method stub
		System.out.println("Seguent VHS");
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		System.out.println("Pausar VHS");
	}

	@Override
	public void previous() {
		// TODO Auto-generated method stub
		System.out.println("Anterior VHS");
	}

	
	
}
