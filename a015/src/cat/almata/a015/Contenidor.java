package cat.almata.a015;

public class Contenidor {
	private String material;

	//Atribut fruit de la relacio
	private Refresc refresc1;
	
	//	Metodes de negoci
	public void afegirRefresc(Refresc r) {
		this.refresc1 = r;
	}
	
	public Refresc obtenirRefresc() {
		return refresc1;
	}
	
	/*Sol agafa el sabor del refresc, 
	en canvi el de dalt agafa el sabor i el id
	*/
	public String getSabor(){
		return refresc1.getSabor();
	}
	
	
	
	
	
	
	
	public Contenidor(String material) {
		super();
		setMaterial(material);
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}
	
}
