package cat.almata.a007;

public class Ppal {

	public static void main(String[] args) {
		Persona p1,p2;
		Quadrat c1, c2;
		
		p1 = new Persona();
		System.out.println("Contador: "+Persona.getContadorPersones());
		p2 = new Persona();
		System.out.println("Contador: "+Persona.getContadorPersones());

		c1 = new Quadrat(3);
		System.out.println("Area quadrat: "+c1.area());
		System.out.println("Area quadrat: "+Quadrat.area(4));
		
		c2 = new Quadrat(5);
		Quadrat c3 = new Quadrat(c2) ;
	}

}
