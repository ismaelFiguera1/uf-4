package cat.almata.a013;

public class Dvd extends Reproductor {
	private String support;

	

	public Dvd(String material, String support) {
		super(material);
		setSupport(support);
	}

	public String getSupport() {
		return support;
	}

	public void setSupport(String support) {
		this.support = support;
	}
	
	



	@Override
	public String toString() {
		return "Dvd [support=" + support + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Li d'ono al play amb el mando de la Tele.");
	}
	
	@Override
	public void stop() {
		// TODO Auto-generated method stub
		System.out.println("Parar DVD");
	}

	@Override
	public void next() {
		// TODO Auto-generated method stub
		System.out.println("Seguent DVD");
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		System.out.println("Pausar DVD");
	}

	@Override
	public void previous() {
		// TODO Auto-generated method stub
		System.out.println("Anterior DVD");
	}


	
	
	
	
}
