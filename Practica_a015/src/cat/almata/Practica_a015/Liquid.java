package cat.almata.Practica_a015;

public class Liquid {
	private String nom;

	public Liquid(String nom) {
		super();
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
}
