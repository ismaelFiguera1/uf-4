package cat.almata.a012;

public class Ppal {

	public static void main(String[] args) {
		Reproductor r1 = new Reproductor("Plastic");
		Reproductor r2 = new Dvd("Plastic", "Media");
		Reproductor r3 = new Vhs("Acer", "Roig"); 
	
		descripcioReproductors(r1);
		playReproductors(r1);
		descripcioReproductors(r2);
		playReproductors(r2);
		descripcioReproductors(r3);
		playReproductors(r3);

	}
	
	public static void playReproductors(Reproductor r) {
		r.play();
	}
	
	public static void descripcioReproductors(Reproductor r) {
		System.out.println(r);
	}

}
