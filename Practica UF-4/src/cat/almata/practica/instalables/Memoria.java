package cat.almata.practica.instalables;

import cat.almata.practica.PlacaBase.XipSet;

public class Memoria implements InstalablePB {

	private double frequencia;
	private String tipusRAM;
	private String id;
	
	
	
	
	public Memoria(double frequencia, String tipusRAM, String id) {
		super();
		setFrequencia(frequencia);
		setTipusRAM(tipusRAM);
		setId(id);
	}




	public double getFrequencia() {
		return frequencia;
	}




	public void setFrequencia(double frequencia) {
		this.frequencia = frequencia;
	}




	public String getTipusRAM() {
		return tipusRAM;
	}




	public void setTipusRAM(String tipusRAM) {
		this.tipusRAM = tipusRAM;
	}




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	@Override
	public String toString() {
		return "Memoria [frequencia=" + frequencia + ", tipusRAM=" + tipusRAM + ", id=" + id + "]";
	}




	@Override
	public boolean isConnectable(XipSet x, Object a) {
		boolean ser = false;
		double frequenca = Double.parseDouble(x.getFrequenciaMemoria());
		Memoria m1 = (Memoria) a;
		if(m1.getFrequencia()==frequenca) {
			if(x.getTipusMemoria().equals(m1.getTipusRAM())) ser = true;
		}
		
		return ser;
	}

}
