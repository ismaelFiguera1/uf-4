package cat.almata.a003;

public class Computadora {
	private int ram;
	private String processador;
	private int id;
	
	//	Constructors
	public Computadora() {
		
	}
	
	public Computadora(int ram, int id) {
		this.setRam(ram);
		this.setId(id);
	}
	
	public Computadora(int ram, String processador, int id) {
		this.setRam(ram);
		this.setProcessador(processador);
		this.setId(id);
	}
	
	
	//	Getters ans setters

	public int getRam() {
		return ram;
	}
	public void setRam(int ram) {
		if(ram < 64) this.ram=ram;
		else this.ram=0;
	}
	public String getProcessador() {
		return processador;
	}
	public void setProcessador(String processador) {
		this.processador = processador;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	//	Funcions
	
	public void apagar() {
		System.out.println("M'estic apagant");
	}
	
	public void iniciar() {
		System.out.println("M'estic iniciant");
	}
	
	public void reiniciar() {
		apagar();
		iniciar();
	}
}
