package cat.almata.practica;

import cat.almata.practica.instalables.Memoria;
import cat.almata.practica.instalables.Microprocessador;
import cat.almata.practica.instalables.Tarjeta_Grafica;

public class PlacaBase {
	private int ranuresPCI;
	private String id;
	
	private static XipSet xipSet=null;
	private static PlacaBase placaBase;
	
	public static final String MISS_WARNING_MEMORIA_ERROR="La memoria no es compatible, canvia-la.";
	public static final String MISS_WARNING_TARJA_ERROR="La targeta grafica no es compatible, canvia-la.";
	public static final String MISS_WARNING_MICRO_ERROR="El microprocessador no es compatible, canvial.";
	public static final String MISS_INFO_CORRECTE="Tots els instalables son correctes i compatibles.";
	
	
	
	
	public static final String MICROERROR = "Ja hi ha un Microprocessador instalat, esborra el que hi ha i fica el nou.";
	public static final String MICRO = "Perfecte, el microprocessador ja esta instalat";
	
	public static final String TARJAERROR = "Ja hi ha una tarjeta grafica instalat, esborra el que hi ha i fica el nou.";
	public static final String TARJA = "Perfecte, la targeta grafica ja esta instalada";
	
	public static final String MEMORIAERROR = "Ja hi ha una memoria instalada, esborra el que hi ha i fica el nou.";
	public static final String MEMORIA = "Perfecte, la memoria ja esta instalada";
	
	private Microprocessador micro=null;
	private Tarjeta_Grafica tarja=null;
	private Memoria memoria=null;
	
	public int instalarMicroprocessador(Microprocessador m1) {
		int error = 0;
		if(micro==null) {
			this.micro=m1;
		}else error=1;
		return error;
	}
	
	public static void infoMicroprocessador(int error) {
		switch (error) {
		case 0:	System.out.println(MICRO);break;
		case 1:	System.out.println(MICROERROR);break;
		}
	}
	
	public void eliminarMicroprocessador() {
		this.micro=null;
	}
	
	public void eliminarTarjetaGrafica() {
		this.tarja=null;
	}
	
	public void eliminarMemoria() {
		this.memoria=null;
	}
	
	public int instalarTargeta_Grafica(Tarjeta_Grafica t1) {
		int error = 0;
		if(tarja==null) {
			this.tarja=t1;
		}else error=1;
		return error;
	}
	
	public static void infoTarja_Grafica(int error) {
		switch (error) {
		case 0:	System.out.println(TARJA);break;
		case 1:	System.out.println(TARJAERROR);break;
		}
	}
	
	public int instalarMemoria(Memoria m1) {
		int error = 0;
		if(memoria==null) {
			this.memoria=m1;
		}else error=1;
		return error;
	}
	
	public static void infoMemoria(int error) {
		switch (error) {
		case 0:	System.out.println(MEMORIA);break;
		case 1:	System.out.println(MEMORIAERROR);break;
		}
	}
	
	
	public int comprobarInstalables() {
		int error = 0;
		if(micro.isConnectable(xipSet, micro)==true) {
			if(tarja.isConnectable(xipSet, tarja)==true) {
				if(memoria.isConnectable(xipSet, memoria)==false) error = 3;
			}else {
				error = 2;
			}
		}else{
			error = 1;
		}
		return error;
	}
	
	public static void infoerrorComprobar(int error) {
		switch (error) {
		case 0: System.out.println(MISS_INFO_CORRECTE);break;
		case 1: System.out.println(MISS_WARNING_MICRO_ERROR);break;
		case 2: System.out.println(MISS_WARNING_TARJA_ERROR);break;
		case 3: System.out.println(MISS_WARNING_MEMORIA_ERROR);break;
		}
	}

	
	
	
	
	
	

	public PlacaBase() {
		placaBase=this;
		xipSet = XipSet.obtenirXipSet();
	}
	
	public static XipSet getXipSet() {
		return xipSet;
	}

	public static PlacaBase getPlacaBase() {
		return placaBase;
	}

	



	public int getRanuresPCI() {
		return ranuresPCI;
	}
	public void setRanuresPCI(int ranuresPCI) {
		this.ranuresPCI = ranuresPCI;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	
	public class XipSet {
		private String socolProcessador;
		private String tipusProcessador;
		private String tipusBusGrafica;
		private String frequenciaMemoria;
		private String tipusMemoria;
		private String connectorDiscDur;
		private String connectorMonitor;
		private String connectorTeclat;
		private String connectorDVD;
		private String connectorRatoli;
		private String connectorFont;
		
		private XipSet() {
			socolProcessador="LGA1366";
			tipusProcessador="Intel Core i7";
			tipusBusGrafica="AGP";
			frequenciaMemoria="1333";
			tipusMemoria="DDR4";
			connectorDiscDur="Sata";
			connectorMonitor="HDMI";
			connectorTeclat="PS2";
			connectorDVD="Sata";
			connectorRatoli="USB";
			connectorFont="32 pins";
		}

		public static XipSet obtenirXipSet() {
			if(xipSet==null) xipSet = placaBase.new XipSet();
			return xipSet;
		}

		
		
		
		
		
		
		
		
		public String getSocolProcessador() {
			return socolProcessador;
		}

		public String getTipusProcessador() {
			return tipusProcessador;
		}

		public String getTipusBusGrafica() {
			return tipusBusGrafica;
		}

		public String getFrequenciaMemoria() {
			return frequenciaMemoria;
		}

		public String getTipusMemoria() {
			return tipusMemoria;
		}

		public String getConnectorDiscDur() {
			return connectorDiscDur;
		}

		public String getConnectorMonitor() {
			return connectorMonitor;
		}

		public String getConnectorTeclat() {
			return connectorTeclat;
		}

		public String getConnectorDVD() {
			return connectorDVD;
		}

		public String getConnectorRatoli() {
			return connectorRatoli;
		}

		public String getConnectorFont() {
			return connectorFont;
		}
	}
	
	
}
