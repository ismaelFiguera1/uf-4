package cat.almata.a015;

import java.util.ArrayList;

public class Caixa {
	private String material;
	private String id;
	
	
	
	// Atribut de la relacio
	ArrayList<Contenidor> caixa= new ArrayList<Contenidor>();
	
	
	//	Metodes de negoci
	
	//Magafa la botella(Contenidor) i la fica a la caixa
	public void addBotella(Contenidor botella) {
		caixa.add(botella);
	}
	
	public ArrayList<Contenidor> getCaixa() {
		return caixa;
	}
	
	
	public Caixa(String material, String id) {
		super();
		this.material = material;
		this.id = id;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
