package cat.almata.a008;

public class Esportista extends Persona {
	private String esport;

	public Esportista(String esport) {
		this.esport = esport;
	}
	
	public Esportista(String esport, String nom, String dni) {
		super(nom,dni);
		setEsport(esport);
	}

	public String getEsport() {
		
		return esport;
	}

	public void setEsport(String esport) {
		this.esport = esport;
	}

	@Override
	public String toString() {
		return "Esport=" + esport + ", " + super.toString() + "";
	}

	
	
	
}
