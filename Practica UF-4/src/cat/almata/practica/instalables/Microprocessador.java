package cat.almata.practica.instalables;

import cat.almata.practica.PlacaBase.XipSet;

public  class Microprocessador implements InstalablePB {
	private double velocitat;
	private String tipusSocol;
	private String tipusProcessador;
	private String id;
	
	
	
	
	




	public Microprocessador(double velocitat, String tipusSocol, String tipusProcessador, String id) {
		super();
		setVelocitat(velocitat);
		setTipusSocol(tipusSocol);
		setTipusProcessador(tipusProcessador);
		setId(id);
	}




	public double getVelocitat() {
		return velocitat;
	}




	public void setVelocitat(double velocitat) {
		this.velocitat = velocitat;
	}




	public String getTipusSocol() {
		return tipusSocol;
	}




	public void setTipusSocol(String tipusSocol) {
		this.tipusSocol = tipusSocol;
	}




	public String getTipusProcessador() {
		return tipusProcessador;
	}




	public void setTipusProcessador(String tipusProcessador) {
		this.tipusProcessador = tipusProcessador;
	}




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	@Override
	public boolean isConnectable(XipSet xip, Object micro) {
		boolean a=false;
		if(xip.getSocolProcessador().equals(((Microprocessador) micro).getTipusSocol())) {
			if(xip.getTipusProcessador().equals(((Microprocessador) micro).getTipusProcessador())) a=true;
		}
		return a;
	}




	@Override
	public String toString() {
		return "Microprocessador [velocitat=" + velocitat + ", tipusSocol=" + tipusSocol + ", tipusProcessador="
				+ tipusProcessador + ", id=" + id + "]";
	}
	
	

}
