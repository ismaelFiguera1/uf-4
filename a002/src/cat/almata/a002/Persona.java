package cat.almata.a002;

public class Persona {
	
	private String nom;
	private String cognom;
	private int edat;
	private float altura;
	private boolean esCasat;

	//	Constructor
	public Persona() {
		System.out.println("Instanciant la persona...");
	}
	

	
	
	public Persona(String nom, int edat, float altura) {
		setNom(nom);
		setEdat(edat);
		setAltura(altura);
	}
	
	public Persona(String nom) {
		setNom(nom);
	}




	//	metodes accessors als atributs de la classe (setters & getters)
	
	public void menjar() {
		System.out.println("La persona esta menjant...");
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getCognom() {
		return cognom;
	}



	public void setCognom(String cognom) {
		this.cognom = cognom;
	}



	public int getEdat() {
		return edat;
	}



	public void setEdat(int unaEdat) {
		if(unaEdat<120) this.edat = unaEdat;
		else this.edat=0;
	}



	public float getAltura() {
		return altura;
	}



	public void setAltura(float alt) {
		if (alt < 235) this.altura = alt;
		else this.altura = 0;
	}



	public boolean isEsCasat() {
		return esCasat;
	}



	public void setEsCasat(boolean esCasat) {
		this.esCasat = esCasat;
	}
	
	
	
	
}
