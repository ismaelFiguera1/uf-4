package cat.almata.a009;

public class Elefant extends Animal {
	private double midaTrompa;

	public Elefant(int edat, double pes, String nom, int id, boolean esViu, double midaTrompa) {
		super(edat, pes, nom, id, esViu);
		setMidaTrompa(midaTrompa);
	}

	public Elefant(int id) {
		super(id);
	}

	public Elefant(double midaTrompa) {
		super();
		setMidaTrompa(midaTrompa);
	}
	
	public Elefant(Animal animal, double midaTrompa) {
		super(animal.getEdat(),animal.getPes(), animal.getNom(), animal.getId(), animal.isEsViu());
		setMidaTrompa(midaTrompa);
	}

	
	
	public double getMidaTrompa() {
		return midaTrompa;
	}

	public void setMidaTrompa(double midaTrompa) {
		if (midaTrompa<250) this.midaTrompa = midaTrompa;
		else this.midaTrompa = 0;
	}

	@Override
	public String toString() {
		return "Elefant [midaTrompa=" + midaTrompa + ", toString()=" + super.toString() + "]";
	}


	
}
