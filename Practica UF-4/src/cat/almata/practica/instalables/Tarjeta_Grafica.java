package cat.almata.practica.instalables;

import cat.almata.practica.PlacaBase.XipSet;

public class Tarjeta_Grafica implements InstalablePB {
	private String tipusBus;
	private String fabricant;
	private String resolucioMaxima;
	private String sortides;
	private String id;
	
	
	
	public Tarjeta_Grafica(String tipusBus, String fabricant, String resolucioMaxima, String sortides, String id) {
		super();
		setTipusBus(tipusBus);
		setFabricant(fabricant);
		setId(id);
		setResolucioMaxima(resolucioMaxima);
		setSortides(sortides);
	}
	
	
	
	
	public String getTipusBus() {
		return tipusBus;
	}
	public void setTipusBus(String tipusBus) {
		this.tipusBus = tipusBus;
	}
	public String getFabricant() {
		return fabricant;
	}
	public void setFabricant(String fabricant) {
		this.fabricant = fabricant;
	}
	public String getResolucioMaxima() {
		return resolucioMaxima;
	}
	public void setResolucioMaxima(String resolucioMaxima) {
		this.resolucioMaxima = resolucioMaxima;
	}
	public String getSortides() {
		return sortides;
	}
	public void setSortides(String sortides) {
		this.sortides = sortides;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}




	@Override
	public String toString() {
		return "Tarjeta_Grafica [tipusBus=" + tipusBus + ", fabricant=" + fabricant + ", resolucioMaxima="
				+ resolucioMaxima + ", sortides=" + sortides + ", id=" + id + "]";
	}




	@Override
	public boolean isConnectable(XipSet xip, Object targetaGrafica) {
		boolean a= false;
		Tarjeta_Grafica t1 = (Tarjeta_Grafica) targetaGrafica;
		if(xip.getTipusBusGrafica().equals(t1.getTipusBus())) {
			a=true;
		}
		
		return a;
	}


	
	

}
