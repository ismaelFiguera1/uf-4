package cat.almata.a013;

public abstract class Reproductor implements Reproduir{
	private String material;
	
	

	public Reproductor(String material) {
		super();
		setMaterial(material);
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	@Override
	public String toString() {
		return "Reproductor [material=" + material + "]";
	}
	
	@Override
	public abstract void play();

	@Override
	public abstract void stop();

	@Override
	public abstract void next();
	
	@Override
	public abstract void pause();

	@Override
	public abstract void previous();


	
	
}
