package cat.almata.a014;

public class Persona {
	private String nom;
	
	public Persona (String nom) {
		this.setNom(nom);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
