package cat.almata.practica.conectables;

import cat.almata.practica.PlacaBase.XipSet;

public class Ratoli implements ConnectablePC {

	private String marca;
	private String connector;
	private String id;
	
	
	
	
	public Ratoli(String marca, String connector, String id) {
		super();
		this.marca = marca;
		this.connector = connector;
		this.id = id;
	}


	


	public String getMarca() {
		return marca;
	}





	public void setMarca(String marca) {
		this.marca = marca;
	}





	public String getConnector() {
		return connector;
	}





	public void setConnector(String connector) {
		this.connector = connector;
	}





	public String getId() {
		return id;
	}





	public void setId(String id) {
		this.id = id;
	}





	@Override
	public String toString() {
		return "Ratoli [marca=" + marca + ", connector=" + connector + ", id=" + id + "]";
	}





	@Override
	public boolean isConnectable(XipSet x, Object obj) {
		boolean ser;
		Ratoli r1 = (Ratoli) obj;
		if(r1.getConnector().equals(x.getConnectorRatoli())) {
			ser = true;
		}else {
			ser = false;
		}
		return ser;
	}

}
