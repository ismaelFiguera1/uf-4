package cat.almata.a011;

public class Professional extends Persona {
	private int dorsal;

	public int getDorsal() {
		return dorsal;
	}

	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}

	public Professional(String nom, String dni, int dorsal) {
		super(nom, dni);
		setDorsal(dorsal);
	}

	@Override
	public void correr() {
		System.out.println("Estic corrent a 15 km hora");
	}




	
	
	
}
